package com.blood.kurudhi;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity
{
    private Toolbar mToolbar;
    private EditText donorNameEditText,donorAgeEditText,donorEmailEditText,donorMobileEditText;
    private Button maleButton,femaleButton,saveButton;
    private String gender;
    private  String bloodGroup;
    NiceSpinner bloodGroupSpinner;
    DataBaseHandler db;
    List<String> dataset=new LinkedList<String>();
    String nl = System.getProperty("line.separator");
    String sno="null";
  static  boolean isUpdatetask;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        this.setContentView(R.layout.activity_main);

        mToolbar=findViewById(R.id.app_bar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
         db=new DataBaseHandler(MainActivity.this);

        Toast.makeText(this, db.getDatabaseName(), Toast.LENGTH_SHORT).show();
isUpdatetask=false;

        donorNameEditText=findViewById(R.id.edittext_name);
        donorAgeEditText=findViewById(R.id.edittext_age);
        donorEmailEditText=findViewById(R.id.edittext_email);
        donorMobileEditText=findViewById(R.id.edittext_mobile);
        maleButton=findViewById(R.id.button_male);
        femaleButton=findViewById(R.id.button_female);
        saveButton=findViewById(R.id.button_save);


        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));
        if(!isFileReadPermissiongranted())
        {
            showPermissionRequestAlertmessage();
        }

         bloodGroupSpinner = (NiceSpinner) findViewById(R.id.nice_spinner);
        dataset = new LinkedList<>(Arrays.asList("A+", "A-", "B+", "B-", "AB+","AB-","O+","O-"));
        bloodGroupSpinner.attachDataSource(dataset);

       // Toast.makeText(this, niceSpinner.getItemAtPosition(niceSpinner.getSelectedIndex()).toString(), Toast.LENGTH_SHORT).show();
        bloodGroup=bloodGroupSpinner.getItemAtPosition(bloodGroupSpinner.getSelectedIndex()).toString();


        bloodGroupSpinner.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                bloodGroup=parent.getItemAtPosition(position).toString();
            }
        });



        maleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender="M";
                maleButton.setBackground(getResources().getDrawable(R.drawable.round_button_selected));
                femaleButton.setBackground(getResources().getDrawable(R.drawable.round_button));
            }
        });

        femaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender="F";
                femaleButton.setBackground(getResources().getDrawable(R.drawable.round_button_selected));
                maleButton.setBackground(getResources().getDrawable(R.drawable.round_button));


            }
        });
 saveButton.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v)
     {
         final String donorName=donorNameEditText.getText().toString();
         final String donorAge=donorAgeEditText.getText().toString();
         final String donorGender=gender;
         final String donorBlood=bloodGroup;
         final String donorEmail=donorEmailEditText.getText().toString();
         final String donorMobileNo=donorMobileEditText.getText().toString();

         new AlertDialog.Builder(MainActivity.this)
                 .setTitle(getResources().getString(R.string.alert_dialog_title))
                 .setMessage(donorName+nl+donorGender+nl+donorAge+nl+donorBlood+nl+donorEmail+nl+donorMobileNo)

                 .setPositiveButton(getResources().getString(R.string.alert_positive_button), new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         if(isUpdatetask)
                         {
                             db.updateDonor(sno,donorName,donorAge,donorGender,donorBlood,donorEmail,donorMobileNo);

                         }
                         else
                         {
                             db.insertDonor(donorName,donorAge,donorGender,donorBlood,donorEmail,donorMobileNo);

                         }

                     }
                 })
                 .setNegativeButton(getResources().getString(R.string.alert_negative_button), new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         dialog.dismiss();
                     }
                 })
                 .setCancelable(false)
                 .show();

         donorNameEditText.getEditableText().clear();
         donorAgeEditText.getEditableText().clear();
         donorEmailEditText.getEditableText().clear();
         donorMobileEditText.getEditableText().clear();
         femaleButton.setBackground(getResources().getDrawable(R.drawable.round_button));
         maleButton.setBackground(getResources().getDrawable(R.drawable.round_button));





     }
 });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater configMenuInflater=getMenuInflater();
        configMenuInflater.inflate(R.menu.nav_menu,menu);
        return  true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.edit_menu:
                startActivity(new Intent(MainActivity.this,ViewDonors.class));
             break;
            case R.id.about_menu:
                break;
            case R.id.switch_language:


        }
        return super.onOptionsItemSelected(item);
    }



    private void showPermissionRequestAlertmessage()
    {
        AlertDialog.Builder alertDialog= new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.permission_request_title))
                .setMessage(getResources().getString(R.string.permission_request_message))
                .setPositiveButton(getResources().getString(R.string.permission_request_positive_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showPermissionRequestDialog();
                    }
                });
        alertDialog.show();
    }
    private boolean isFileReadPermissiongranted()
    {
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void showPermissionRequestDialog()
    {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                2);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case  2:
                if((grantResults.length>0)&&grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(this, getResources().getString(R.string.permission_request_success), Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(this, getResources().getString(R.string.permission_request_failure), Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        isUpdatetask=false;

        try{

            Bundle extras = getIntent().getExtras();
             sno=extras.getString("sno");
            Log.d("navv",sno);

            List<String> donorDetails=db.getDonorDetail(sno);

            donorNameEditText.setText(donorDetails.get(1));
            donorAgeEditText.setText(donorDetails.get(3));
            if(donorDetails.get(2).equals("M"))
            {
                gender="M";
                maleButton.setBackground(getResources().getDrawable(R.drawable.round_button_selected));
                femaleButton.setBackground(getResources().getDrawable(R.drawable.round_button));
            }
            if(donorDetails.get(2).equals("F"))
            {
                gender="F";
                femaleButton.setBackground(getResources().getDrawable(R.drawable.round_button_selected));
                maleButton.setBackground(getResources().getDrawable(R.drawable.round_button));
            }

            bloodGroupSpinner.setSelectedIndex(dataset.indexOf(donorDetails.get(4)));

            donorEmailEditText.setText(donorDetails.get(5));
            donorMobileEditText.setText(donorDetails.get(6));
            isUpdatetask=true;


        }
        catch (Exception ex)
        {
            Log.e("exception",ex.getMessage());
        }

    }
}
