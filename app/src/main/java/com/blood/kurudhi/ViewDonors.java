package com.blood.kurudhi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;


public class ViewDonors extends AppCompatActivity
{

    private RecyclerView recyclerView;
    DonorsListAdapter donorsListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_donors);



        DataBaseHandler db=new DataBaseHandler(this);
        List<List> donorList=db.getDonorsList();

        recyclerView=findViewById(R.id.recycler_view_donor_list);

        recyclerView.setLayoutManager(new LinearLayoutManager(ViewDonors.this));
        recyclerView.setHasFixedSize(false);
        donorsListAdapter=new DonorsListAdapter(donorList,ViewDonors.this);

        recyclerView.setAdapter(donorsListAdapter);

    }
}
