package com.blood.kurudhi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DataBaseHandler extends SQLiteOpenHelper {

    private static final String DBName ="blood.db" ;
    private static final int DATABASE_VERSION =1 ;

    public DataBaseHandler(final Context context)
    {
        super(context, Environment.getExternalStorageDirectory() + File.separator + "kurudhi" + File.separator + DBName, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String blood = "CREATE TABLE DONORS (S_NO INTEGER PRIMARY KEY,DONOR_NAME TEXT,DONOR_GENDER " + "TEXT, DONOR_AGE TEXT,DONOR_BLOOD_GROUP TEXT,DONOR_EMAIL TEXT,DONOR_MOBILE TEXT)";
        db.execSQL(blood);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void insertDonor(String donnorName,String donorAge,String donorGender,String donorBloodGroup,String donorEmail,String donorMobileNo)

    {

        SQLiteDatabase mdb=this.getWritableDatabase();
        mdb.setLocale(new Locale("en_US"));

        ContentValues values=new ContentValues();




        values.put("S_NO",getSno());
        values.put("DONOR_NAME",donnorName);
        values.put("DONOR_AGE",donorAge);
        values.put("DONOR_GENDER",donorGender);
        values.put("DONOR_BLOOD_GROUP",donorBloodGroup);
        values.put("DONOR_EMAIL",donorEmail);
        values.put("DONOR_MOBILE",donorMobileNo);
    mdb.insert("DONORS",null,values);
    mdb.close();


    }

    public String getSno() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.setLocale(new Locale("en_US"));

        long count = DatabaseUtils.queryNumEntries(db, "DONORS");
        Log.d("insert", String.valueOf(count));
        String sno = null;
        if (count == 0) {
            sno = "1";
        } else {
            String selectQuery = "SELECT  S_NO FROM DONORS";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToLast()) {
                Integer value = Integer.valueOf(cursor.getString(0)) + 1;
                sno = String.format("%05d", value);
            }


        }

        return sno;

    }

    public List<List> getDonorsList() {
        List<List> donors = new ArrayList<List>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.setLocale(new Locale("en_US"));

            String query_get_bills = "select * from  DONORS  order by S_NO DESC";
            Cursor cursor = db.rawQuery(query_get_bills, null);


            while (cursor.moveToNext()) {
                List<String> donorDetail = new ArrayList<String>();
                donorDetail.add(cursor.getString(1));
                donorDetail.add(cursor.getString(3));
                donorDetail.add(cursor.getString(4));
                donorDetail.add(cursor.getString(0));
                //Log.d("saniyan", cursor.getString(0));
                donors.add(donorDetail);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return donors;

    }
    public List<String> getDonorDetail(String sNo) {
        List<String> donorDetail = new ArrayList<String>();

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.setLocale(new Locale("en_US"));

            String query_get_bills = "select * from  DONORS  where S_NO="+sNo;
            Cursor cursor = db.rawQuery(query_get_bills, null);


            while (cursor.moveToNext()) {
                donorDetail = new ArrayList<String>();
                donorDetail.add(cursor.getString(0));
                donorDetail.add(cursor.getString(1));
                donorDetail.add(cursor.getString(2));
                donorDetail.add(cursor.getString(3));
                donorDetail.add(cursor.getString(4));
                donorDetail.add(cursor.getString(5));
                donorDetail.add(cursor.getString(6));
                //Log.d("saniyan", cursor.getString(0));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return donorDetail;

    }


    public void updateDonor(String sNo,String donnorName,String donorAge,String donorGender,String donorBloodGroup,String donorEmail,String donorMobileNo)

    {

        SQLiteDatabase mdb=this.getWritableDatabase();
        mdb.setLocale(new Locale("en_US"));


        ContentValues values=new ContentValues();




        values.put("S_NO",sNo);
        values.put("DONOR_NAME",donnorName);
        values.put("DONOR_AGE",donorAge);
        values.put("DONOR_GENDER",donorGender);
        values.put("DONOR_BLOOD_GROUP",donorBloodGroup);
        values.put("DONOR_EMAIL",donorEmail);
        values.put("DONOR_MOBILE",donorMobileNo);
        mdb.update("DONORS",values,"S_NO="+sNo,null);
        mdb.close();


    }



}
