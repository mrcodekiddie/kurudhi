package com.blood.kurudhi;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DonorsListAdapter extends RecyclerView.Adapter<DonorsListAdapter.ViewHolder>
{
    List<List> donorsList;
    Context context;

    public DonorsListAdapter(List<List> donorList, Context context) {
        this.donorsList=donorList;
        this.context=context;
    }

    @Override
    public DonorsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_donor, viewGroup, false);
        return  new DonorsListAdapter.ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final DonorsListAdapter.ViewHolder holder, int position)
    {
        List<List> donor=donorsList;
        List<String> donorPart=donor.get(position);
        holder.donorNameTextView.setText(donorPart.get(0));
        holder.donorAgeTextView.setText(donorPart.get(1));
        holder.donorBloodGroupTextView.setText(donorPart.get(2));
        holder.donorSnoTextView.setText(donorPart.get(3));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(context, "I am num "+String.valueOf(position)+" item", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("sno",holder.donorSnoTextView.getText().toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return donorsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView donorNameTextView,donorAgeTextView,donorBloodGroupTextView,donorSnoTextView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            donorNameTextView=itemView.findViewById(R.id.list_donor_name);
            donorAgeTextView=itemView.findViewById(R.id.list_donor_age);
            donorBloodGroupTextView=itemView.findViewById(R.id.list_donor_blood);
            donorSnoTextView=itemView.findViewById(R.id.list_donor_sno);
        }
    }

}
